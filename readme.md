## Front-End Home page

![Home page](https://www.dropbox.com/s/tl7hr42z03boz90/laptop_home.png?dl=1)

## Front-End Login page

![Login](https://www.dropbox.com/s/ruvwt7rebporlz2/laptop_login.png?dl=1)

## Front-End Add form page

![Add](https://www.dropbox.com/s/2j6lo9fkrv78dco/laptop_form.png?dl=1)

## Front-End Add View page

![Add View](https://www.dropbox.com/s/3mdyfgy9pknluod/laptop_view.png?dl=1)

## Front-End Responsive

![Mobile](https://www.dropbox.com/s/ukvwedyojou7gkm/mobile.png?dl=1)
