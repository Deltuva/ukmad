<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('frontHome');

Auth::routes();

// Advertising
Route::get('/skelbimai/{category}', 'Advertising\CategoryController@categoryIndex')->name('advertisingCategory');
Route::get('/skelbimai/{category?}/{subcategory}', 'Advertising\SubCategoryController@subcategoryIndex')->name('advertisingSubCategory');
Route::get('/skelbimai/{category?}/{subcategory?}/{parent}', 'Advertising\ChildSubCategoryController@subsubcategoryIndex')->name('advertisingSubChildCategory');
Route::get('/skelbimas/{adId}-{slug}', 'Advertising\ItemController@show')->name('advertisingItemView');
Route::get('/ideti-skelbima', 'Advertising\ItemCreatingController@createForm')->name('advertisingCreateForm');
Route::get('/paieska', 'Advertising\SearchController@searchResult')->name('search');

// Static pages
Route::view('/taisykles', 'static-pages.rules');
Route::view('/reklama', 'static-pages.reklama');
Route::view('/kontaktai', 'static-pages.contact');
Route::view('/duk', 'static-pages.duk');