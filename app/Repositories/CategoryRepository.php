<?php
namespace App\Repositories;

use App\Category;

class CategoryRepository
{
    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }
    
    public function getCategories()
    {
        return $this->model->all();
    }

    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }
}