<?php
namespace App\Repositories;

use App\Location;

class LocationRepository
{
    protected $model;

    public function __construct(Location $location)
    {
        $this->model = $location;
    }
    
    public function getLocations()
    {
        return $this->model->all();
    }

    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }
}