<?php
namespace App\Repositories;

use App\SubCategory;

class SubCategoryRepository
{
    protected $model;

    public function __construct(SubCategory $subcategory)
    {
        $this->model = $subcategory;
    }
    
    public function getCategories($category)
    {
        return $this->model
          ->where('cat_id', $category)
          ->get();
    }

    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }
}