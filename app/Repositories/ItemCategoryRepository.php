<?php

namespace App\Repositories;

use App\Item;

class ItemCategoryRepository
{
    const ITEMS_LIMIT = 10;
    const ITEMS_IN_SEARCH_LIMIT = 10;

    protected $model;

    public function __construct(Item $advertisingItem)
    {
        $this->model = $advertisingItem;
    }

    /**
     * Get all items by url request
     *
     * @param [Key] $key
     * @param [Category] $category
     * @param [Location] $location
     *
     * @return mixed
     */
    public function getRecentSearchResultsByKey($key = null, $category = null, $location = null)
    {
        $columns = [
            'advertising_items.id AS id',
            'advertising_items.title',
            'advertising_items.description',
            'advertising_items.slug',
            'advertising_items.created_at AS created_at',
            'advertising_items.updated_at AS updated_at',
        ];

        $qr = $this->model
            ->select($columns);

        $qr->where('location_id', $location);
        $qr->whereHas('category_item', function ($queryCategoryItem) use ($category, $location) {
            $queryCategoryItem
                    ->where('cat_id', $category);
        })->where(function ($query) use ($key) {
            $query->orWhere('title', 'like', '%' . $key . '%')
                      ->orWhere('description', 'like', '%' . $key . '%');
        });
        $qr->orderedTimeByCreated(); // created time

        $query = $qr->paginate(self::ITEMS_IN_SEARCH_LIMIT);

        return $query;
    }

    /**
     * Get all items by category
     *
     * @param [Category] $category
     *
     * @return mixed
     */
    public function getCatItems($category = null)
    {
        $columns = [
            'advertising_items.id AS id',
            'advertising_items.title',
            'advertising_items.description',
            'advertising_items.slug',
            'advertising_items.created_at AS created_at',
            'advertising_items.updated_at AS updated_at',
        ];

        $qr = $this->model
            ->select($columns);

        $qr->whereHas('category_item', function ($queryCategoryItem) use ($category) {
            $queryCategoryItem
                    ->where('cat_id', $category);
        });
        $qr->orderedTimeByCreated(); // created time

        $query = $qr->paginate(self::ITEMS_LIMIT);

        return $query;
    }

    /**
     * Get all items by Subcategory
     *
     * @param [SubCategory] $subcategory
     *
     * @return mixed
     */
    public function getSubcatItems($subcategory = null)
    {
        $columns = [
            'advertising_items.id AS id',
            'advertising_items.title',
            'advertising_items.description',
            'advertising_items.slug',
            'advertising_items.created_at AS created_at',
            'advertising_items.updated_at AS updated_at',
        ];

        $qr = $this->model
            ->select($columns);

        $qr->whereHas('category_item', function ($queryCategoryItem) use ($subcategory) {
            $queryCategoryItem
                    ->where('sub_id', $subcategory);
        });
        $qr->orderedTimeByCreated(); // created time

        $query = $qr->paginate(self::ITEMS_LIMIT);

        return $query;
    }

    /**
     * Get all items by SubSubcategory
     *
     * @param [SubSubCategory] $subsubcategory
     *
     * @return mixed
     */
    public function getSubSubcatItems($subsubcategory = null)
    {
        $columns = [
            'advertising_items.id AS id',
            'advertising_items.title',
            'advertising_items.description',
            'advertising_items.slug',
            'advertising_items.created_at AS created_at',
            'advertising_items.updated_at AS updated_at',
        ];

        $qr = $this->model
            ->select($columns);

        $qr->whereHas('category_item', function ($queryCategoryItem) use ($subsubcategory) {
            $queryCategoryItem
                    ->where('sub_sub_id', $subsubcategory);
        });
        $qr->orderedTimeByCreated(); // created time

        $query = $qr->paginate(self::ITEMS_LIMIT);

        return $query;
    }
}
