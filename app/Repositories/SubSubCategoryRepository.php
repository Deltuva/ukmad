<?php
namespace App\Repositories;

use App\SubSubCategory;

class SubSubCategoryRepository
{
    protected $model;

    public function __construct(SubSubCategory $subsubcategory)
    {
        $this->model = $subsubcategory;
    }
    
    public function getCategories($subcategory)
    {
        return $this->model
          ->where('sub_id', $subcategory)
          ->get();
    }

    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }
}