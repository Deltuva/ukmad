<?php

namespace App\Repositories;

use App\Item;
use App\Events\ViewItemCounterEvent;

class ItemRepository
{
    const LATEST_LIMIT_ITEMS = 6;

    protected $model;

    public function __construct(Item $advertisingItem)
    {
        $this->model = $advertisingItem;
    }

    /**
     * Get all items by category
     * 
     * @param [Category] $category
     * 
     * @return void
     */
    public function getItems($category = 1)
    {
        return $this->model->categories->find($category)->get();
    }

    public function getCountActiveItems($is_active) {

        $query = $this->model
            ->where('is_active', $is_active)
            ->count();

    	return $query;
    }
    
    /**
     * Get all published items
     * 
     * @param boolean $is_active
     * 
     * @return mixed
     */
    public function getRecentItems($is_active = true) {

        $columns = [
            'advertising_items.id AS id',
            'advertising_items.title',
            'advertising_items.description',
            'advertising_items.slug',
            'advertising_items.created_at AS created_at',
            'advertising_items.updated_at AS updated_at',
        ];

        $query = $this->model
            ->select($columns)
            ->orderedTimeByCreated()
            ->take(self::LATEST_LIMIT_ITEMS)
            ->where('is_active', $is_active)
            ->get();

        //event(new ViewItemCounterEvent($this->model));

    	return $query;
	}
	
}