<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $table = 'advertising_items_categories';

    public function items()
    {
        return $this->hasMany('App\Item', 'id', 'advertising_id');
    }

}
