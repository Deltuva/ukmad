<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';

    public function adCategory()
    {
        return $this->hasMany('App\ItemCategory', 'sub_id');
    }

    public function getCountOfCategoriesAtAttribute()
    {
        return $this->adCategory ?
       (int) $this->adCategory->count() : 0;
    }
}