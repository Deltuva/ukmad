<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'slug'];

    public function subcategories()
    {
        return $this->hasMany('App\SubCategory', 'cat_id', 'id');
    }

    public function adCategory()
    {
        return $this->hasMany('App\ItemCategory', 'cat_id');
    }

    public function getCountOfCategoriesAtAttribute()
    {
        return $this->adCategory ?
       (int) $this->adCategory->count() : 0;
    }
}