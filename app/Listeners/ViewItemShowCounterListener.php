<?php

namespace App\Listeners;

use App\Events\ViewItemShowCounterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ViewItemShowCounterListener
{
    /**
     * Handle the event.
     *
     * @param  ViewItemShowCounterEvent  $event
     * @return void
     */
    public function handle(ViewItemShowCounterEvent $event)
    {
        if ( ! $this->isItemViewed($event->item))
        {
            $event->item->increment('view_show_counter');

            $this->storeItem($event->item);
        }
    }

    private function isItemViewed($item)
    {
        $viewed = request()
            ->session()
            ->get('viewed_items', []);

        return in_array($item->id, $viewed);
    }

    private function storeItem($item)
    {    
        request()
            ->session()
            ->push('viewed_items', $item->id);
    }
}
