<?php

namespace App\Listeners;

use App\Events\ViewItemCounterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ViewItemCounterListener
{
    /**
     * Handle the event.
     *
     * @param  ViewItemCounterEvent  $event
     * @return void
     */
    public function handle(ViewItemCounterEvent $event)
    {
        $event->item->increment('view_counter');
    }
}
