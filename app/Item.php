<?php

namespace App;

use Carbon\Carbon;
use App\Traits\Advertising\StatusShow;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use StatusShow;

    const DIFF_IN_DAYS = 1;
    const DIFF_IN_MINUTES = 5;

    protected $table = 'advertising_items';

    public function category_item()
    {
        return $this->hasMany('App\ItemCategory', 'advertising_id', 'id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getTodayDateStatus()
    {
        $t = new Carbon($this->created_at, 'Europe/Vilnius');

        $status = __('<span class="td">Šiandien</span>');
        $statusWhenNull = Carbon::parse($this->created_at)->format('d/m/d');
        $todayStatus = ($t->diff(Carbon::now())->days < self::DIFF_IN_DAYS) ? $status : $statusWhenNull;

        return $todayStatus;
    }

    public function getTimeAgoDays()
    {
        setlocale(LC_TIME, 'Lithuania');
        Carbon::setLocale('lt');
        $createdDate = new Carbon($this->updated_at, 'Europe/Vilnius');
        //$now = Carbon::now();
        $date = Carbon::createFromTimeStamp(strtotime($createdDate))->diffForHumans();
        if ($createdDate->diffInDays() < self::DIFF_IN_DAYS) {
            $agoTime = $createdDate->diffForHumans();
            $replacedAgoTime = ucfirst(str_replace(['valandą', 'valandas', 'valandų'], 'val.', $agoTime));
    
            $iconWidget = $replacedAgoTime;
    
        } else {
            $agoTime = 'Prieš '.$createdDate->diffInDays().' d.';
            $iconWidget = $agoTime;
        }
        $agoStatus = $iconWidget;
        return $agoStatus;
    }

    public function getHowManyShowCounter()
    {
        return $this->view_show_counter > 0 ? $this->view_show_counter : 'Nan';
    }

    public function getIsFree()
    {
        return $this->is_free;
    }

    public function getPriceIsZero()
    {
        return $this->price_zero;
    }

    public function getContactName()
    {
        return $this->name;
    }

    public function getContactPhone()
    {
        return $this->phone;
    }

    public function getContactEmail()
    {
        return $this->email;
    }

    public function getContactSkype()
    {
        return $this->skype;
    }

    public function getContactAddress()
    {
        return $this->address;
    }

    public function getContactVideo()
    {
        return $this->video;
    }

    public function getContactWebsite()
    {
        return $this->website;
    }

    public function getTimeAgoEditedStatus()
    {
        $updatedDate = new Carbon($this->updated_at, 'Europe/Vilnius');

        $editedStatus = ($updatedDate->diffInMinutes() <= self::DIFF_IN_MINUTES) ? __('<span class="ed">Atnaujintas</span>') : null;

        return $editedStatus;
    }

    public function scopeOrderedTimeByCreated($query)
    {
        return $query->latest();
    }

    public function scopeOrderedTimeByUpdated($query)
    {
        return $query->orderBy('updated_at', 'DESC');
    }
}
