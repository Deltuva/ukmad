<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\ItemRepository;

class HomeController extends Controller
{
    protected $categoryRepo;
    protected $adItemRepo;

    /**
     * [Controller constructor]
     *
     * @param CategoryRepository $categoryRepo
     * @param ItemRepository $adItemRepo
     */
    public function __construct(
        CategoryRepository $categoryRepo,
        ItemRepository $adItemRepo
    )
    {
        $this->categoryRepo = $categoryRepo;
        $this->adItemRepo = $adItemRepo;
    }

    /**
     * Home page
     */
    public function index()
    {
        $categories          = $this->categoryRepo->getCategories();
        $active_advertisings = $this->adItemRepo->getCountActiveItems(true);
        $advertisings        = $this->adItemRepo->getRecentItems();

        return view('home', [
            'categories' => $categories,
            'active_advertisings' => $active_advertisings,
            'advertisings' => $advertisings
        ]);
    }
}