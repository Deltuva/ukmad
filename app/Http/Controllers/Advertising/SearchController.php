<?php

namespace App\Http\Controllers\Advertising;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchQueryRequest;
use App\Repositories\ItemCategoryRepository;

class SearchController extends Controller
{
    protected $adRepo;
    protected $frontDir = 'advertising/';

    /**
     * [Controller constructor]
     *
     * @param ItemCategoryRepository $adRepo
     */
    public function __construct(
        ItemCategoryRepository $adRepo
      ) {
        $this->adRepo = $adRepo;
    }

    /**
     * Get Search by
     *
     * @param SearchQueryRequest $request
     *
     * @return mixed
     */
    public function searchResult(SearchQueryRequest $request)
    {
        $searchQueryKey = $request->input('qkey');
        $searchQueryCategoryId = $request->input('qcat');
        $searchQueryLocation = $request->input('qloc');

        $queryIsValid = $request->validated();
        if ($queryIsValid) {
            $title = __("Skelbimų paieška - {$searchQueryKey}");

            $searchResult = $this->adRepo->getRecentSearchResultsByKey($searchQueryKey, $searchQueryCategoryId, $searchQueryLocation);

            return view($this->frontDir . 'search.search-result', [
                'title' => $title,
                'selectedCategoryType' => $searchQueryCategoryId,
                'selectedLocationType' => $searchQueryLocation,
                'results' => $searchResult
            ]);
        }
    }
}
