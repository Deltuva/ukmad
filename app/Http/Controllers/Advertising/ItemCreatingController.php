<?php

namespace App\Http\Controllers\Advertising;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemCreatingController extends Controller
{
    protected $frontDir = 'advertising/';

    public function createForm(Request $request) {

        return view($this->frontDir. 'forms.create');
    }
}