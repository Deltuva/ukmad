<?php

namespace App\Http\Controllers\Advertising;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\SubCategoryRepository;
use App\Repositories\SubSubCategoryRepository;
use App\Repositories\ItemCategoryRepository;

class SubCategoryController extends Controller
{
    protected $categoryRepo;
    protected $subcategoryRepo;
    protected $subsubcategoryRepo;
    protected $adRepo;
    protected $frontDir = 'advertising/';

    /**
     * [Controller constructor]
     *
     * @param CategoryRepository $categoryRepo
     * @param SubCategoryRepository $subcategoryRepo
     * @param SubSubCategoryRepository $subsubcategoryRepo
     * @param ItemCategoryRepository $adRepo
     */
    public function __construct(
        CategoryRepository $categoryRepo,
        SubCategoryRepository $subcategoryRepo,
        SubSubCategoryRepository $subsubcategoryRepo,
        ItemCategoryRepository $adRepo
      )
    {
          $this->categoryRepo = $categoryRepo;
          $this->subcategoryRepo = $subcategoryRepo;
          $this->subsubcategoryRepo = $subsubcategoryRepo;
          $this->adRepo = $adRepo;
    }

    public function subcategoryIndex(Request $request, $category_slug = false, $sub_category_slug = false, $sub_sub_category_slug = false) {
        
        // Categories
        $category       = $this->categoryRepo->findIdBySlug($category_slug);
        $subcategory    = $this->subcategoryRepo->findIdBySlug($sub_category_slug);
        $subcategories  = $this->subsubcategoryRepo->getCategories($subcategory->id);

        // Ads
        $items          = $this->adRepo->getSubcatItems($subcategory->id);

        return view($this->frontDir. 'sub_categories.advertising-list', [
            'category'                      => $category,
            'subcategory'                   => $subcategory,
            'subsubcategories'              => $subcategories,
            'category_advertising_items'    => $items
          ]);
    }
}