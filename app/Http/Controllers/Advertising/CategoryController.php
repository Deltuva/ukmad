<?php

namespace App\Http\Controllers\Advertising;

use App\Category;
use App\ItemCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\SubCategoryRepository;
use App\Repositories\ItemCategoryRepository;
use App\Repositories\SubSubCategoryRepository;

class CategoryController extends Controller
{
    protected $categoryRepo;
    protected $subcategoryRepo;
    protected $adRepo;
    protected $frontDir = 'advertising/';

    /**
     * [Controller constructor]
     *
     * @param CategoryRepository $categoryRepo
     * @param SubCategoryRepository $subcategoryRepo
     * @param ItemCategoryRepository $adRepo
     */
    public function __construct(
        CategoryRepository $categoryRepo,
        SubCategoryRepository $subcategoryRepo,
        ItemCategoryRepository $adRepo
      )
    {
          $this->categoryRepo = $categoryRepo;
          $this->subcategoryRepo = $subcategoryRepo;
          $this->adRepo = $adRepo;
    }

    public function categoryIndex(Request $request, $category_slug) {

        // Categories
        $category       = $this->categoryRepo->findIdBySlug($category_slug);
        $categories     = $this->subcategoryRepo->getCategories($category->id);

        // Ads
        $items          = $this->adRepo->getCatItems($category->id);

        return view($this->frontDir. 'categories.advertising-list', [
            'category'                      => $category,
            'subcategories'                 => $categories,
            'category_advertising_items'    => $items
          ]);
    }
}