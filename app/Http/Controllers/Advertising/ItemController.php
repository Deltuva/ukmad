<?php

namespace App\Http\Controllers\Advertising;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Advertising\SearchFirst;
use App\Events\ViewItemShowCounterEvent;
use App\Repositories\ItemCategoryRepository;
use App\Http\Requests\Advertising\ItemShowRequest;

class ItemController extends Controller
{
    use SearchFirst;
    
    protected $adRepo;
    protected $frontDir = 'advertising/';

    /**
     * [Controller constructor]
     *
     * @param ItemCategoryRepository $adRepo
     */
    public function __construct(ItemCategoryRepository $adRepo)
    {
        $this->adRepo = $adRepo;
    }

    public function show($itemId, $itemSlug) {

        $advertising = $this->singleSenseById([
            'id'    => $itemId,
            'slug'  => $itemSlug
        ]);
        event(new ViewItemShowCounterEvent($advertising));

        return view($this->frontDir. 'module.includes.advertising-view', [
            'advertising' => $advertising
        ]);
    }
}