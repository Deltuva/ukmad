<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Location;
use Cache;

class LocationComposer
{
    /**
     * @var $location
     */
    protected $location;
    /**
     * Constructor
     */
    public function __construct(Location $location)
    {
        $this->locations = $location;
    }
    /**
     * Get Home Categories
     * 
     * @param  View  $view
     */
    public function compose(View $view)
    {   
        $locations = Cache::rememberForever('locationsFromComposer', function () {
            return $this->locations
                ->onlySelected()
                ->get();
        });
        $view->with('locationsFromComposer', $locations);
    }
}