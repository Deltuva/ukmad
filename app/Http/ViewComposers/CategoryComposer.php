<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Category;
use Cache;

class CategoryComposer
{
    /**
     * @var $category
     */
    protected $category;
    /**
     * Constructor
     */
    public function __construct(Category $category)
    {
        $this->categories = $category;
    }
    /**
     * Get Home Categories
     * 
     * @param  View  $view
     */
    public function compose(View $view)
    {   
        $categories = Cache::rememberForever('categoriesFromComposer', function () {
            return $this->categories->get();
        });
        $view->with('categoriesFromComposer', $categories);
    }
}