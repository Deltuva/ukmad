<?php
namespace App\Helpers;

use App\Category;
use App\SubCategory;
use App\SubSubCategory;

class WebsiteBread
{
    public static function getBreadcrumbs()
    {
        if (count(request()->segments())) {
            for ($segmentsInumberPosition = 1; $segmentsInumberPosition <= count(request()
                ->segments()); $segmentsInumberPosition++) {

                if ($segmentsInumberPosition == 1) {
                    $breadArray[$segmentsInumberPosition] = [
                        'segment' => 1,
                        'title' => 'Pradžia',
                        'url' => url('/'),
                    ];
                } elseif ($segmentsInumberPosition == 2) {
                    $categoryName = Category::whereSlug(self::currentSegment(2))
                        ->first();

                    $breadArray[$segmentsInumberPosition] = [
                        'segment' => 2,
                        'title' => $categoryName->name,
                        'url' => route('advertisingCategory', self::currentSegment(2)),
                    ];
                } elseif ($segmentsInumberPosition == 3) {
                    $categoryName = SubCategory::whereSlug(self::currentSegment(3))
                        ->first()
                        ->name;

                    $breadArray[$segmentsInumberPosition] = [
                        'segment' => 3,
                        'title' => $categoryName,
                        'url' => route('advertisingSubCategory', [
                            self::currentSegment(2),
                            self::currentSegment(3),
                        ]),
                    ];
                } elseif ($segmentsInumberPosition == 4) {
                    $categoryName = SubSubCategory::whereSlug(self::currentSegment(4))
                        ->first()
                        ->name;

                    $breadArray[$segmentsInumberPosition] = [
                        'segment' => 4,
                        'title' => $categoryName,
                        'url' => route('advertisingSubChildCategory', [
                            self::currentSegment(2),
                            self::currentSegment(3),
                            self::currentSegment(4),
                        ]),
                    ];
                } else {
                    $breadArray = [];
                }
            }
        }
        return $breadArray;
    }

    public static function currentSegment($segment)
    {
        if ($segment) {
            return request()->segment($segment);
        }
    }
}
