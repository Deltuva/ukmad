<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $fillable = ['name', 'selected'];

    public function scopeOnlySelected($query, $selected = true)
    {
        return $query->where('is_selected', $selected);
    }
}
