<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
    protected $table = 'sub_sub_categories';

    public function adCategory()
    {
        return $this->hasMany('App\ItemCategory', 'sub_sub_id');
    }

    public function getCountOfCategoriesAtAttribute()
    {
        return $this->adCategory ?
       (int) $this->adCategory->count() : 0;
    }
}
