<?php

namespace App\Traits\Advertising;

trait StatusShow
{
    public function isViewed($itemId)
    {
        if (!$this->isItemViewed($itemId)) {
            return false;
        }

        return true;
    }

    public function isItemViewed($itemId)
    {
        $viewed = request()
            ->session()
            ->get('viewed_items', []);

        return in_array($itemId, $viewed);
    }
}
