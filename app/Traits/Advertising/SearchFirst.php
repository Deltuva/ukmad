<?php

namespace App\Traits\Advertising;

use App\Item;

trait SearchFirst
{
    /**
     * Get Item detail
     * 
     * @param array $params
     * 
     * @return void
     */
    protected function singleSenseById(array $params) {

        $columns = [
            '*',
        ];

        $model = new Item();
        $item = $model
            ->select($columns)
            ->where($params)
            ->first();

        if (!$item) {
            abort(404);
        }

        return $item;
    }
}