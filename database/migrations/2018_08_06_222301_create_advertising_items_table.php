<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('slug')->nullable();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('location_id')->index();
            $table->unsignedInteger('type_id')->index();
            $table->boolean('is_active')->default(false);
            $table->boolean('is_free')->default(false);
            $table->integer('view_counter')->default(0);
            $table->integer('view_show_counter')->default(0);
            $table->string('name')->nullable();
            $table->boolean('name_is_hide')->default(false);
            $table->string('phone')->nullable();
            $table->boolean('phone_is_hide')->default(false);
            $table->string('email')->nullable();
            $table->boolean('email_is_hide')->default(false);
            $table->string('skype')->nullable();
            $table->boolean('skype_is_hide')->default(false);
            $table->string('address')->nullable();
            $table->boolean('address_is_hide')->default(false);
            $table->string('video')->nullable();
            $table->boolean('video_is_hide')->default(false);
            $table->string('website')->nullable();
            $table->boolean('website_is_hide')->default(false);
            $table->boolean('price_zero')->default(false);
            $table->double('price', 10, 2);
            $table->date('deadline');
            $table->date('premium_deadline');
            $table->char('code', 30);
            $table->string('ip', 20)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising_items');
    }
}
