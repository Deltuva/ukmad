<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisingItemsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_items_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('advertising_id')->index();
            $table->unsignedInteger('cat_id')->index();
            $table->unsignedInteger('sub_id')->index();
            $table->unsignedInteger('sub_sub_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising_items_categories');
    }
}
