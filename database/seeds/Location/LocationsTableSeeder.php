<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Location name / if selected
        $locations = [
            'Vilnius' => false,
            'Kaunas' => false,
            'Ukmergė' => true,
        ];

        foreach ($locations as $location_name => $selected) {
            Location::create([
                'name' => $location_name,
                'is_selected' => $selected
            ]);
        }
    }
}