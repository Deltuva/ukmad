<?php

use App\Category;
use Carbon\Carbon;
use App\SubCategory;
use App\SubSubCategory;
use App\Item;
use Illuminate\Database\Seeder;
use App\ItemCategory;

class AdvertisingItemsTableSeeder extends Seeder
{
    const ITEMS_LIMIT = 1000;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < self::ITEMS_LIMIT; $i++) { 
            $category_id = Category::take(1)->first();
            $sub_category_id = SubCategory::whereCatId($category_id->id)->take(1)->first()->id;
            $sub_sub_category_id = SubSubCategory::inRandomOrder()->whereSubId($sub_category_id)->take(1)->first()->id;

            $advertising = new Item();

            $add = $advertising->create([
                'title' => $faker->sentence($nbWords = 3, $varaiblenbWords = true),
                'description' => $faker->text(100),
                'slug' => str_slug($faker->sentence($nbWords = 6, $varaiblenbWords = true). '-' . uniqid() . '-'),
                'user_id' => $faker->numberBetween($min = 1, $max = 100),
                'location_id' => $faker->numberBetween($min = 1, $max = 10),
                'type_id' => $faker->numberBetween(1, 3),
                'is_active' => $faker->numberBetween(1, 0),
                'is_free' => $faker->numberBetween(1, 0),
                'name' => $faker->name,
                'name_is_hide' => 0,
                'phone' => $faker->e164PhoneNumber,
                'phone_is_hide' => 0,
                'email' => $faker->email,
                'email_is_hide' => 0,
                'skype' => 'developer.tectuslt',
                'skype_is_hide' => 0,
                'address' => $faker->address,
                'address_is_hide' => 0,
                'video' => $faker->url,
                'video_is_hide' => 0,
                'website' => $faker->url,
                'website_is_hide' => 0,
                'price_zero' => $faker->numberBetween(1, 0),
                'price' => $faker->numberBetween(100, 500),
                'deadline' => Carbon::now()->addMonths(1),
                'premium_deadline' => Carbon::now()->addDay(3),
                'code' => $faker->password,
                'ip' => $faker->ipv4
            ]);

            if ($add) {
                $item_category = new ItemCategory();

                $item_category->create([
                    'advertising_id' => $add->id,
                    'cat_id' => $category_id->id,
                    'sub_id' => $sub_category_id,
                    'sub_sub_id' => $sub_sub_category_id
                ]);
                
            }
        }
    }
}
