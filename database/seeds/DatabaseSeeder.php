<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Categories Tree
        $this->call(CategoriesTableSeeder::class);
        $this->command->info("Categories table seeded.");
        $this->call(SubCategoriesTableSeeder::class);
        $this->command->info("Sub Categories table seeded.");
        $this->call(SubParentCategoriesTableSeeder::class);
        $this->command->info("Sub Parent Categories table seeded.");

        // Locations
        $this->call(LocationsTableSeeder::class);
        $this->command->info("Locations table seeded.");

        // Advertising
        $this->call(AdvertisingItemsTableSeeder::class);
        $this->command->info("Advertising Items table seeded.");
    }
}