<?php

use App\SubSubCategory;
use Illuminate\Database\Seeder;

class SubParentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locale_lt = 'lt';

        /*******************************************************
         ** ------------ AUTOMOBILIAI ------------------------*
         ******************************************************/

        $parent_automobiliai_lt = [
            'Automobilių supirkimas',
            'Alfa romeo',
            'Audi',
            'BMW',
            'Chevrolet',
            'Chrysler',
            'Citroen',
            'Fiat',
            'Ford',
            'Honda',
            'Hyundai',
            'Jaguar',
            'Jeep',
            'Kia',
            'Lexus',
            'Mazda',
            'Mercedes',
            'Mitsubishi',
            'Nissan',
            'Opel',
            'Peugeot',
            'Porsche',
            'Renault',
            'Seat',
            'Škoda',
            'Subaru',
            'Suzuki',
            'Toyota',
            'Volkswagen',
            'Volvo',
            'Kita markė'
        ];

        $parent_paslaugos_lt = [
            'Remontas',
            'Nuoma',
            'Transporto paslaugos',
            'Kita'
        ];
        
        foreach ($parent_automobiliai_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 1,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Paslaugos
        foreach ($parent_paslaugos_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 5,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        /*******************************************************
         ** ------------ NT ------------------------*
         ******************************************************/

        $parent_statybos_paslaugos_lt = [
            'Statybiniai darbai',
            'Apdaila',
            'Santechnikos darbai',
            'Aplinkos tvarkymas',
            'Elektros instaliacija',
            'Kita'
        ];

        $parent_medziagos_iranga_lt = [
            'Statybinės medžiagos',
            'Statybos įranga',
            'Kita'
        ];

        $parent_nuoma_lt = [
            'Butai',
            'Namai, kotedžai',
            'Komercinės patalpos',
            'Garažai'
        ];

        // Statybos paslaugos
        foreach ($parent_statybos_paslaugos_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 13,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Medžiagos įranga
        foreach ($parent_medziagos_iranga_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 14,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Nuoma
        foreach ($parent_nuoma_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 18,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        /****************************************************************
         ** -------------------------- TECHNIKA ---------------------- **
         ***************************************************************/

        $parent_mob_telefonai_lt = [
            'Apple',
            'Blackberry',
            'HTC',
            'Huawei',
            'LG',
            'Nokia',
            'Samsung',
            'Sony',
            'Klonai, kopijos',
            'Dalys, priedai',
            'Paslaugos',
            'Kita'
        ];

        $parent_kompiuteriai_lt = [
            'Nešiojami',
            'Stacionarūs',
            'Planšetiniai',
            'Žaidimų',
            'Kompiuterių komponentai',
            'Priedai, išoriniai įrenginiai',
            'Paslaugos',
            'Kita'
        ];

        $parent_buitine_technika_lt = [
            'Smulki buitinė technika',
            'Skalbimo mašinos',
            'Šaldytuvai',
            'Dulkių siurbliai',
            'Paslaugos',
            'Kita'
        ];

        $parent_buitine_technika_lt = [
            'Smulki buitinė technika',
            'Skalbimo mašinos',
            'Šaldytuvai',
            'Dulkių siurbliai',
            'Paslaugos',
            'Kita'
        ];

        // Mobilieji telefonai
        foreach ($parent_mob_telefonai_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 27,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Kompiuteriai
        foreach ($parent_kompiuteriai_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 28,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Buitine technika
        foreach ($parent_buitine_technika_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 29,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        /********************************************************************
         ** -------------------------- LAISVALAIKIS ---------------------- **
         ********************************************************************/

        $parent_suaugusiems_lt = [
            'Mergina ieško',
            'Vaikinas ieško',
            'Fetišas'
        ];

        // Suaugusiems, pažintys
        foreach ($parent_suaugusiems_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 47,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        /**************************************************************************
         ** -------------------------- DRABUŽIAI, AVALYNĖ ---------------------- **
         **************************************************************************/

        $parent_moterims_lt = [
            'Apatinis trikotažas',
            'Rūbai',
            'Avalynė',
            'Kita'
        ];

        $parent_vyrams_lt = [
            'Apatinis trikotažas',
            'Rūbai',
            'Avalynė',
            'Kita'
        ];

        // Moterims
        foreach ($parent_moterims_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 49,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        // Vyrams
        foreach ($parent_vyrams_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 50,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

        /**************************************************************************
         ** -------------------------- AUGINANTIEMS VAIKUS --------------------- **
         **************************************************************************/

        $parent_drabuziai_avalyne_lt = [
            'Mergaitėms',
            'Berniukams'
        ];

        // Drabužiai, avalynė
        foreach ($parent_drabuziai_avalyne_lt as $value) {
            SubSubCategory::create([
                'sub_id' => 53,
                'parent_id' => false,
                'name' => $value,
                'slug' => str_slug($value, '-'),
                'locale' => $locale_lt
            ]);
        }

    }
}