<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Category name / icon class
        $categories_lt = [
            'Transportas' => 'cat-01',
            'Nekilnojamas turtas' => 'cat-02',
            'Paslaugos, darbas' => 'cat-03',
            'Technika' => 'cat-04',
            'Buitis' => 'cat-05',
            'Laisvalaikis' => 'cat-06',
            'Drabužiai, avalynė' => 'cat-07',
            'Auginantiems vaikus' => 'cat-08'
        ];

        //each all categories from array
        $categories_locale = 'lt';

        foreach ($categories_lt as $categories_lt_value => $category_icon) {
            Category::create([
                'name' => $categories_lt_value,
                'icon' => $category_icon,
                'slug' => str_slug($categories_lt_value, '-'),
                'locale' => $categories_locale
            ]);
        }
    }
}