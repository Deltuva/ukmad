<?php

use App\SubCategory;
use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories_transportas_lt = [
            'Automobiliai',
            'Žemės ūkio technika',
            'Dviračiai',
            'Dalys, aksesuarai',
            'Paslaugos',
            'Motociklai, motoroleriai',
            'Spec. transportas',
            'Mikroautobusai',
            'Kiti'
        ];

        $subcategories_nt_lt = [
            'Butai',
            'Namai',
            'Sklypai',
            'Statybos paslaugos',
            'Medžiagos, įranga',
            'Patalpos',
            'Garažai',
            'Sodybos',
            'Nuoma',
            'Kiti'
        ];

        $subcategories_paslaugos_darbas_lt = [
            'Siūlo darbą',
            'Ieško darbo',
            'Grožio, sveikatos paslaugos',
            'Renginių paslaugos',
            'Verslo paslaugos',
            'Kursai, mokymai',
            'Kiti'
        ];

        $subcategories_technika_lt = [
            'Mobilieji telefonai',
            'Kompiuteriai',
            'Buitinė technika',
            'Pramoninė technika',
            'Video technika',
            'Sodui, daržui, miškui',
            'Audio technika',
            'Kiti'
        ];

        $subcategories_buitis_lt = [
            'Baldai, interjeras',
            'Flora, fauna',
            'Grožis, sveikata',
            'Namų apyvokos reikmenys',
            'Kietas, skystas kuras',
            'Antikvariatas',
            'Paslaugos',
            'Kiti'
        ];

        $subcategories_laisvalaikis_lt = [
            'Sportas, žaidimai',
            'Muzika, instrumentai',
            'Žvejyba, medžioklė',
            'Kelionės, turizmas',
            'Suaugusiems, pažintys',
            'Kiti'
        ];

        $subcategories_drabuziai_lt = [
            'Moterims',
            'Vyrams',
            'Papuošalai, aksesuarai',
            'Kiti'
        ];

        $subcategories_auginantiems_vaikus_lt = [
            'Drabužiai, avalynė',
            'Baldai',
            'Vežimėliai, kėdutės',
            'Žaislai',
            'Kiti'
        ];

        //each all subcategories from array
        $subcategories_locale = 'lt';

        // Transportas
        foreach ($subcategories_transportas_lt as $subcategories_transportas_lt_value) {
            SubCategory::create([
                'cat_id' => 1,
                'parent_id' => false,
                'name' => $subcategories_transportas_lt_value,
                'slug' => str_slug($subcategories_transportas_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Nekilnojamas turtas
        foreach ($subcategories_nt_lt as $subcategories_nt_lt_value) {
            SubCategory::create([
                'cat_id' => 2,
                'parent_id' => false,
                'name' => $subcategories_nt_lt_value,
                'slug' => str_slug($subcategories_nt_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Paslaugos darbas
        foreach ($subcategories_paslaugos_darbas_lt as $subcategories_paslaugos_darbas_lt_value) {
            SubCategory::create([
                'cat_id' => 3,
                'parent_id' => false,
                'name' => $subcategories_paslaugos_darbas_lt_value,
                'slug' => str_slug($subcategories_paslaugos_darbas_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Technika
        foreach ($subcategories_technika_lt as $subcategories_technika_lt_value) {
            SubCategory::create([
                'cat_id' => 4,
                'parent_id' => false,
                'name' => $subcategories_technika_lt_value,
                'slug' => str_slug($subcategories_technika_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Buitis
        foreach ($subcategories_buitis_lt as $subcategories_buitis_lt_value) {
            SubCategory::create([
                'cat_id' => 5,
                'parent_id' => false,
                'name' => $subcategories_buitis_lt_value,
                'slug' => str_slug($subcategories_buitis_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Laisvalaikis
        foreach ($subcategories_laisvalaikis_lt as $subcategories_laisvalaikis_lt_value) {
            SubCategory::create([
                'cat_id' => 6,
                'parent_id' => false,
                'name' => $subcategories_laisvalaikis_lt_value,
                'slug' => str_slug($subcategories_laisvalaikis_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Drabužiai, avalynė
        foreach ($subcategories_drabuziai_lt as $subcategories_drabuziai_lt_value) {
            SubCategory::create([
                'cat_id' => 7,
                'parent_id' => false,
                'name' => $subcategories_drabuziai_lt_value,
                'slug' => str_slug($subcategories_drabuziai_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }

        // Auginantiems vaikus
        foreach ($subcategories_auginantiems_vaikus_lt as $subcategories_auginantiems_vaikus_lt_value) {
            SubCategory::create([
                'cat_id' => 8,
                'parent_id' => false,
                'name' => $subcategories_auginantiems_vaikus_lt_value,
                'slug' => str_slug($subcategories_auginantiems_vaikus_lt_value, '-'),
                'locale' => $subcategories_locale
            ]);
        }
    }
}