<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'UKM') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
  <![endif]-->

    <meta name="application-name" content="UKM" />
    <meta name="msapplication-TileColor" content="#b71542" />
    <link rel="apple-touch-icon" href="{{ asset('images/logo-small.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/logo-small.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/logo-small.png') }}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{ asset('images/logo-small.png') }}">

</head>

<body>


    <div id="app">
        <!-- Top Header nav -->
        @include('includes.header')
        <!-- Top Search form -->
        @include('includes.search')

        @yield('content')

        <!-- Bottom footer -->
        @include('includes.footer')
    </div>

    <!-- Scripts -->
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>


    @stack('scripts')
</body>

</html>