@extends('layouts.app')

@section('content')

@include('includes.breadcrumbs')

<section class="advertisings">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                @include('includes.heading', [
                    'counter' => $subsubcategory->countOfCategoriesAt,
                    'title' => $subsubcategory->name
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                @include('advertising.module.template', $category_advertising_items)
            </div>
        </div>

    </div>
</section>
@endsection