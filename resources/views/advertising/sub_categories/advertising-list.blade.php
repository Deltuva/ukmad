@extends('layouts.app')

@section('content')

@include('includes.breadcrumbs')

@if(!$subsubcategories->isEmpty())
<section class="subcategories">
    <div class="container">
        <div class="row">
            @foreach ($subsubcategories as $subsubcategory)
                <div class="col-12 col-lg-3 col-md-4 col-sm-6">
                    <nav class="subcategories-list">
                        <ul>
                            <li class="subcategories-list--item">
                                <i class="fas fa-chevron-right align-baseline" style="font-size: 11px;"></i>
                                <a href="{{ route('advertisingSubCategory', [
                        ''.$category->slug.'/'.$subcategory->slug, $subsubcategory->slug
                        ]) }}">{{
                                    $subsubcategory->name }} <span>{{ $subsubcategory->countOfCategoriesAt }}</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif
<section class="advertisings">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                @include('includes.heading', [
                    'counter' => $subcategory->countOfCategoriesAt,
                    'title' => $subcategory->name
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                @include('advertising.module.template', $category_advertising_items)
            </div>
        </div>

    </div>
</section>
@endsection