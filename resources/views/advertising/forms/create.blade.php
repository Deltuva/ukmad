@extends('layouts.app')

@section('content')
<section id="content" class="create-advertisings">

    <div class="container">
        <div class="section-header_steps">

            <div class="row">
                <div class="col-4 col-md-4">
                    <div class="media active">
                        <div class="media-left align-self-center">
                            <span class="circle-border">
                                <span class="circle">
                                    1
                                </span>
                            </span>
                        </div>
                        <div class="media-body my-2">
                            <h4>Skelbimo informacija</h4>
                        </div>
                    </div>
                </div>

                <div class="col-4 col-md-4">
                    <div class="media">
                        <div class="media-left align-self-center">
                            <span class="circle-border">
                                <span class="circle">
                                    2
                                </span>
                            </span>
                        </div>
                        <div class="media-body my-2">
                            <h4>Skelbimo nuotraukos</h4>
                        </div>
                    </div>
                </div>

                <div class="col-4 col-md-4">
                    <div class="media">
                        <div class="media-left align-self-center">
                            <span class="circle-border">
                                <span class="circle">
                                    3
                                </span>
                            </span>
                        </div>
                        <div class="media-body my-2">
                            <h4>Parduokite greičiau</h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="section-header_title my-4">
            <h3>{{ __('Įdėti skelbimą') }}</h3>
        </div>

        <div class="section-form mb-5">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf

                <div class="form-group-bg">
                    <div class="form-group row">
                        <div class="col-lg-6 col-md-6">
                            <label for="label_name">
                                {{ __('Skelbimo kategorija') }} <span>*</span>
                            </label>

                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6">
                            <label for="label_name">
                                {{ __('Tarpinė kategorija') }} <span>*</span>
                            </label>

                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>

                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Modelis') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_input">
                                <input id="input" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Kėbulo tipas') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Modelis') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_input">
                                <input id="input" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Kėbulo tipas') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Modelis') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_input">
                                <input id="input" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Kėbulo tipas') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Modelis') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_input">
                                <input id="input" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-12 col-sm-2 text-md-center">
                            <label for="label_name">
                                {{ __('Kėbulo tipas') }} <span>*</span>
                            </label>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="section-form_selection">
                                <select class="cl form-control">
                                    <option selected>{{ _('-- Pasirinkite --') }}</option>
                                    <option value="71">Option name value</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="label_name">
                            {{ __('Skelbimo pavadinimas') }} <span>*</span>
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('Vietovė') }} <span>*</span>
                        </label>

                        <div class="section-form_selection">
                            <select class="cl form-control">
                                <option selected>{{ _('-- Pasirinkite --') }}</option>
                                <option value="71">Option name value</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('Pardavėjas') }} <span>*</span>
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('Telefono numeris') }} <span>*</span>
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('El. pašto adresas') }}
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('Kaina') }}
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-6">
                        <label for="label_name">
                            {{ __('Interneto svetainė') }}
                        </label>

                        <div class="section-form_input">
                            <input id="input" class="form-control" type="text">
                        </div>

                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-sm-12">
                        <label for="label_name">
                            {{ __('Skelbimas') }} <span>*</span>
                        </label>

                        <div class="section-form_input">
                            <textarea class="textarea-height form-control" id="input" rows="5"></textarea>
                        </div>

                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Išsaugoti skelbimą') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</section>
@endsection