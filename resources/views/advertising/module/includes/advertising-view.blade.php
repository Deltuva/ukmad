@extends('layouts.app')

@section('content')

@include('includes.ad-breadcrumbs')

<section class="advertisings-detail mt-3">
	<div class="container">

		<div class="row">
			<div class="col-12 col-lg-5 mb-4">
				<div class="advertisings-detail--images">
					<div class="mb-4">
						<a class="advertisings-detail--images--image" href="#">
							<img class="img-fluid" src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg"
								alt="....">
						</a>
					</div>

					<div class="slick-section">
						<div class="slick-carousel">
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
							<div class="slick-item">
								<a href="#">
									<img src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="Image"
										class="img-fluid">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-lg-7">
				<h1>{{ $advertising->getTitle() }}</h1>

				<div class="advertisings-detail--content mt-3">
					<div class="description mb-4">
						{{ $advertising->getDescription() }}
					</div>

					<div class="price mb-4"> {{ __('Kaina') }}:
						@if (!$advertising->getPriceIsZero())
						<span>{{ $advertising->getPrice() }} {{ __('EUR') }}</span>
						@else
						<span>{{ __('NEMOKAMAI') }}</span>
						@endif
					</div>

					<div class="contacts-section mb-3">
						<strong>{{ __('Kontaktai') }}</strong>

						@if (!$advertising->name_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('Paskelbė') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactName() }}
							</span>
						</div>
						@endif

						@if (!$advertising->phone_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('Telefonas') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactPhone() }}
							</span>
						</div>
						@endif

						@if (!$advertising->skype_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('Skype') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactSkype() }}
							</span>
						</div>
						@endif

						@if (!$advertising->email_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('El. paštas') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactEmail() }}
							</span>
						</div>
						@endif

						@if (!$advertising->video_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('Vaizdo įrašas') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactVideo() }}
							</span>
						</div>
						@endif

						@if (!$advertising->website_is_hide)
						<div class="contacts-section--column">
							<span class="contacts-section--column--label">{{ __('Svetainė') }}:</span>
							<span class="contacts-section--column--info">
								{{ $advertising->getContactWebsite() }}
							</span>
						</div>
						@endif
					</div>

					<div class="statistic-section mb-3">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-6 col-md-6 col-sm-12 mb-3 no-padding">
									<strong>{{ __('Skelbimo informacija') }}</strong>

									@if ($advertising->getId() > 0)
									<div class="statistic-section--column">
										<span class="statistic-section--column--label">{{ __('ID') }}:</span>
										<span class="statistic-section--column--info">
											{{ $advertising->getId() }}
										</span>
									</div>
									@endif

									<div class="statistic-section--column">
										<span class="statistic-section--column--label">{{ __('Atnaujintas') }}:</span>
										<span class="statistic-section--column--info">
											{{ $advertising->getTimeAgoDays() }}
										</span>
									</div>
									<div class="statistic-section--column">
										<span class="statistic-section--column--label">{{ __('Peržiūrėtas') }}:</span>
										<span class="statistic-section--column--info">
											{{ $advertising->getHowManyShowCounter() }}
										</span>
									</div>
								</div>

								<div class="col-12 col-lg-6 col-md-6 col-sm-12 no-padding">
									<div class="row">
										<div class="col-2 col-lg-3 col-md-3 col-sm-3 text-center">
											<a class="like-button" href="#" data-toggle="tooltip" data-placement="bottom"
												title="Įsiminti skelbimą">
												<i class="far fa-heart fa-2x"></i>
											</a>
										</div>
										<div class="col-2 col-lg-3 col-md-3 col-sm-3 text-center">
											<a class="print-button" href="#" data-toggle="tooltip" data-placement="bottom"
												title="Spausdinti skelbimą">
												<i class="fas fa-print fa-2x"></i>
											</a>
										</div>
										<div class="col-2 col-lg-3 col-md-3 col-sm-3 text-center">
											<a class="send-button" href="#" data-toggle="tooltip" data-placement="bottom"
												title="Nusiųsti skelbimą draugui">
												<i class="fas fa-envelope fa-2x"></i>
											</a>
										</div>
										<div class="col-2 col-lg-3 col-md-3 col-sm-3 text-center">
											<a class="report-button" href="#" data-toggle="tooltip" data-placement="bottom"
												title="Pranešti apie skelbimą">
												<i class="far fa-times-circle fa-2x"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="advertisings-detail--comments mt-3 clearfix">
					<div class="d-flex justify-content-between o-comment-form py-4">
						<h1>Komentarai</h1>
						<button type="button" class="btn btn-default">Rašyti komentarą</button>
					</div>

					{{-- @include('advertising.comments.comment-list') --}}

					<div class="comments-section">
						<div class="container">
							<div class="row">
								<div class="col-sm-12 no-padding">
									<div class="col-12 no-padding">
										<div class="comment-block mb-3">
											<div class="row">
												<div class="col-12 col-md-10">
													<div class="comment-block--nickname text-left mb-2">
														<strong class="d-block-inline">Admin</strong>
														<span>(El. p. papjautas{eta}gmail.com)</span>
													</div>

													<div class="comment-block--msg text-left">
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut
														alias inventore vitae
														recusandae, earum quibusdam, fuga blanditiis
														error laboriosam itaque soluta beatae maiores nulla
														perspiciatis?
													</div>
												</div>

												<div class="col-12 col-md-2">
													<div class="comment-block--date text-right">
														<span class="time">2018 Liepos 2d., 23:38</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="s-comment-form">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-6 d-block mx-auto">
									@include('advertising.comments.comment-list')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
@endsection