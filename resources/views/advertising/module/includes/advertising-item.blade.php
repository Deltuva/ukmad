<div class="advertising-item">
    <div class="row">
        <div class="col-4 col-md-2">
            <div class="advertising-item--image">
                <a href="#">
                    <img class="img-fluid" src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="....">
                </a>
            </div>
        </div>
        <div class="col-8 col-md-7 no-padding-left">
            <div class="advertising-item--content">
                <h3 class="mt-1 mb-3">
                    <a target="_blank" href="{{ route('advertisingItemView', [$category_advertising->getId(), $category_advertising->slug]) }}">
                        {{ $category_advertising->getTitle() }}</a>

                    @if ($category_advertising->getIsFree() || $category_advertising->getPriceIsZero())
                        <span class="price ml-2">
                            <span>{{ __('NEMOKAMAI') }}</span>
                        </span>
                    @else
                    
                        @if ($category_advertising->getPrice() > 0)
                            <span class="price ml-2">
                                <span>{{ $category_advertising->getPrice() }} &euro;</span>
                            </span>
                        @endif

                    @endif
                </h3>
                <div class="description d-none-xs">
                    {{ $category_advertising->getDescription() }}
                </div>
            </div>
        </div>
        <div class="col-8 col-md-3 no-padding-left d-none d-lg-block d-md-block">
            <div class="advertising-item--data">
                <div class="d mt-4 text-right">
                    {!! $category_advertising->getTodayDateStatus() !!}

                    <div class="h">
                        {{ \Carbon\Carbon::parse($category_advertising->created_at)->format('H:i') }}
                    </div>
                    {!! $category_advertising->getTimeAgoEditedStatus() !!}

                </div>
            </div>
        </div>
    </div>
</div>