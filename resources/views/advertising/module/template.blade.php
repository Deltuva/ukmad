@each('advertising.module.includes.advertising-item', $category_advertising_items, 'category_advertising',
'advertising.module.includes.advertising-empty')

<div class="pagination-container pagination-m py-4">
    {{ $category_advertising_items->links() }}
</div>