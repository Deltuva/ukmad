@extends('layouts.app') 
@section('content')

<section id="content" class="advertisings">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                @include('includes.heading', [ 'counter' => 0, 'title' => $title ])
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                @each('advertising.module.includes.advertising-item', $results, 'category_advertising', 'advertising.module.includes.advertising-empty')

                <div class="pagination-container pagination-m py-4">

                    @if ($results->lastPage() > 0)
                        {!! $results->appends(request()->query())
                            ->links() !!}
                    @endif

                </div>
            </div>
        </div>

    </div>
</section>
@endsection