<div class="comments-form">
    @if (session()->has('success'))
        <div id="callback">
            <div class="alert alert-success rounded-0 mt-5">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif

    <div class="comment clearfix">

        <div class="form-group row">
            <div class="col-lg-6 col-md-6 mb-1">
                <label for="commentName">{{ __('Jūsų vardas:') }}</label>
                <input id="name" class="form-control" type="text" name="name">
            </div>
            <div class="col-lg-6 col-md-6">
                <label for="commentEmail">{{ __('El. pašto adresas:') }}</label>
                <input id="email" class="form-control" type="text" name="email">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12 col-md-12">
                <label for="commentMsg">{{ __('Komentaras:') }}</label>
                <textarea rows="5" id="comment" class="form-control" name="comment"></textarea>
            </div>
        </div>
        <div class="form-group row pull-right">
            <div class="col-lg-4 col-md-4">
                <button type="submit" class="btn btn-default">Komentuoti</button>
            </div>
        </div>

    </div>
</div>