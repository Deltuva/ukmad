@extends('layouts.app')

@section('content')
<div id="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 no-padding">
                <div class="card border-0">
                    <div class="card-header border-0 font-weight-bold bg-white">
                        {{ __('Kontaktai') }}
                    </div>

                    <div class="card-body pt-0">
                        <p>IV "PAPJAUTAS-DEVELOP"</p>
                        <p>El. p.: papjautas.com@gmail.com</p>
                        <p>Tel. Nr. +370.6******</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection