@extends('layouts.app')

@section('content')
<div id="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 no-padding">
                <div class="card border-0">
                    <div class="card-header border-0 font-weight-bold bg-white">
                        {{ __('Dažnai užduodami klausimai') }}
                    </div>

                    <div class="card-body pt-0">
                        Nėra.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection