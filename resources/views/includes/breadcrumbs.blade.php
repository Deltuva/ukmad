<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="breadcrumbs">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @foreach (\App\Helpers\WebsiteBread::getBreadcrumbs() as $breadcrumb)
                            @if (!is_null($breadcrumb))
                                <li class="breadcrumb-item active" aria-current="page">
                                    <a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>