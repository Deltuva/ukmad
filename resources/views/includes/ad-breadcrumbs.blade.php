<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="breadcrumbs">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Pradžia</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Transportas</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Automobiliai</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Automobilių supirkimas</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Automobiliu supirkimas superkauto.lt</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>