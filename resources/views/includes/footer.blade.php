<footer class="main-footer">
    <div class="container">
        <div class="row">

            <div class="col-12 col-sm-9">
                <nav class="foot-nav my-5">
                    <div class="row">
                        @foreach (config('website.footer_links') as $footTitle => $footRouteLink)
                            <div class="col-12 col-lg-2 col-sm-12 no-padding-right">
                                <a href="{{ $footRouteLink }}">{{ $footTitle }}</a>
                            </div>
                        @endforeach
                    </div>
                </nav>
            </div>

            <div class="col-12 col-sm-3">
                <div class="foot-by text-right my-5">
                    @if (config('website.footer_created_by'))
                        {!! config('website.footer_created_by.author') !!}
                    @endif
                </div>
            </div>

        </div>
    </div>
</footer>