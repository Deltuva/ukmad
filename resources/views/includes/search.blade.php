<div class="search-container py-4">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form method="GET" action="{{ route('search') }}">

                    <div class="form-group row mb-0 no-gutters">
                        <div class="col-12 col-md-5 pr-3">
                            <div class="search-form">
                                <div class="search-form--input">
                                    <input id="qkey" name="qkey" type="text" class="form-control" name="qkey" placeholder="Skelbimų paieška">
                                </div>
                            </div>
                        </div>
												
                        <div class="col-12 col-md-3 pr-3">
                            <div class="search-form">
                                <div class="search-form--input">
                                    <select name="qcat" class="cl form-control">
                                    <option value="" selected>{{ __('Skelbimo kategorija') }}</option>

                                    @if(!$categoriesFromComposer->isEmpty())
                                        @foreach ($categoriesFromComposer as $category)

                                            @if (isset($selectedCategoryType))
                                                <option value="{{ $category->id }}" {{ $selectedCategoryType == $category->id ? 'selected' : ''}}>
                                                    {{ $category->name }}
                                                </option>
                                            @else
                                                <option value="{{ $category->id }}">
                                                    {{ $category->name }}
                                                </option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-10 col-md-3 pl-0 pr-3">
                            <div class="search-form">
                                <div class="search-form--input">
                                    <select name="qloc" class="cl form-control">
                                    <option value="" selected>{{ __('Vietovė') }}</option>

                                    @if(!$locationsFromComposer->isEmpty())
                                        @foreach ($locationsFromComposer as $location)

                                            @if (isset($selectedLocationType))
                                                <option value="{{ $location->id }}" {{ $selectedLocationType == $location->id ? 'selected' : ''}}>
                                                    {{ $location->name }}
                                                </option>
                                            @else
                                                <option value="{{ $location->id }}">
                                                    {{ $location->name }}
                                                </option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-2 col-md-1">
                            <div class="search-form">
                                <div class="search-form--input">
                                    <button type="submit" class="btn btn-primary border-0">
                                    <i class="fas fa-search"></i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>