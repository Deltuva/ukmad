<header class="main-head py-3">

  <div class="container">
    <div class="navbar-container">

      <nav class="navbar navbar-light bg-faded">
        <a class="navbar-brand mobile d-block d-sm-block d-md-none d-lg-none" href="#">
          <img src="{{ asset('img/logo_mobile.png') }}" class="img-fluid d-inline-block align-top" alt="">
        </a>
        <a class="navbar-brand d-none d-md-block d-lg-block" href="#">
          <img src="{{ asset('img/logo_dekstop.png') }}" class="img-fluid d-inline-block align-top" alt="">
        </a>

        <ul class="navbar-nav">
          @auth
            <li class="nav-item dropdown dropdown-menu-right">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownUser" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="avatar rounded-circle mr-2" width="50" src="https://avatars0.githubusercontent.com/u/43092468?s=400&u=f169303bb2fc3eacc8802755f89d5f94c1c7b9d8&v=4">
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownUser">
                    <a class="dropdown-item" href="{{ route('logout') }}">
                        <i class="fas fa-sign-out-alt"></i> Atsijungti
                    </a>
                </div>
            </li>
          @endauth

           @guest
            <div class="nav__mobile d-block d-sm-block d-sm-none d-md-none d-lg-none">
                <li class="nav-item post-skb">
                <a href="#" class="btn btn-primary mr-2">
                    Vartotojų aplinka
                </a>
                <a href="#" class="btn btn-primary border-0">
                    + Įdėti skelbimą
                </a>
                </li>
            </div>
          @endguest

          @guest
            <div class="nav__dekstop d-none d-md-block d-lg-block">
                <li class="nav-item post-skb">
                <a href="#" class="btn btn-primary mr-2">
                    Vartotojų aplinka
                </a>
                <a href="#" class="btn btn-primary border-0">
                    + Įdėti skelbimą
                </a>
                </li>
            </div>
          @endguest

        </ul>
      </nav>
    </div>
  </div>

</header>