@if ($counter > 0)
    <span class="d-block advertising-count py-4">
        {{ __('Skelbimų:') }} <span>{{ $counter }}</span>
    </span>
@endif

@if (!is_null($title))
    <h4 class="py-3">
        <strong>{{ $title }}</strong>
    </h4>
@endif