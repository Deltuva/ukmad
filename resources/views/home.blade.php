@extends('layouts.app')

@section('content')
<main role="main">

	<div class="categories bg-light">

		<div class="topline-container py-5">
			<div class="container">
				@if ($active_advertisings > 0)
            <div class="active-advetisiments">
              {{ __('Duomenų bazėje rodomi aktyvūs skelbimai') }} <span>{{ $active_advertisings }}</span>
            </div>
				@endif
			</div>
		</div>

		<div class="container">
			<div class="row">
				@if(!$categories->isEmpty())
                @foreach ($categories as $category)
                    <div class="col-12 col-lg-3 col-md-4 col-sm-6 pl-1 pr-1">

                        <div class="categories-block">
                            <div class="categories-container mb-4">
                                <div class="categories-body">

                                    @if(!is_null($category->name))
                                        <div class="row">
                                            <div class="col-3 col-sm-3 col-md-4">
                                                <a class="{{ $category->icon }}" href="{{ route('advertisingCategory', $category->slug) }}"></a>
                                            </div>
                                            <div class="col-9 col-sm-9 col-md-8 pl-1">
                                                <h1 class="categories-block--title">
                                                    <a href="{{ route('advertisingCategory', $category->slug) }}">{{ $category->name }}</a>
                                                </h1>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($category->subcategories()->count() > 0)
                                        <ul class="categories-list pt-4">
                                            @foreach($category->subcategories()->get() as $subcategory)
                                                @if(!is_null($subcategory->name))
                                                    <li class="categories-list--item">
                                                        <i class="fas fa-chevron-right align-middle"></i>
                                                        <a href="{{ route('advertisingSubCategory', [
																														$category->slug, $subcategory->slug
																													]) }}">{{ $subcategory->name }}
                                                            <span class="categories-list--item--cb pull-right">{{ $subcategory->countOfCategoriesAt }}</span></a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
              @endforeach
				@endif
			</div>
        </div>
        
        @if(!$advertisings->isEmpty())
            <div class="newest-container py-5">
                <div class="container">
                    @if ($advertisings->count() > 0)
                        <div class="newest-title mb-4">
                            {{ __('Naujausi skelbimai') }}
                        </div>

                        <div class="carousel-container">
                            <div class="newest-carousel">
                                @foreach ($advertisings as $advertising_item)
                                    @php $isShow = $advertising_item->isViewed($advertising_item
																				->getId()) == true ? 'show' : null; @endphp

                                    @if(!is_null($advertising_item->getTitle()))
                                        <div class="slick-item {{ $isShow }}">
                                            <a href="{{ route('advertisingItemView', [$advertising_item->getId(), $advertising_item->slug]) }}">
                                                <img class="img-fluid" src="https://paslaugos-img.dgn.lt/gallery_5_300099/automobiliu-nuoma.jpg" alt="...">
                                            </a>
                                            @if ($isShow)
                                                <div class="time {{ $isShow }}">
                                                    {{ __('PERŽIŪRĖTAS') }}
                                                </div>
                                            @else
                                                <div class="time">
                                                    {!! $advertising_item->getTodayDateStatus() !!}
                                                </div>
                                            @endif
                                            <div class="title-wp">
                                                <span class="title">
                                                    {{ $advertising_item->getTitle() }}
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endif
	</div>

</main>
@endsection