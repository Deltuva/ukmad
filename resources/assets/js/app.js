/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import "./bootstrap";
import "./libs/fontawesome_v5";
import "./libs/slick_carousel";

/**
 * Comments
 */
import "./modules/comment/comment";

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	el: "#app"
});

$(function() {
	$("span.services-count").hide();
	$('[data-toggle="tooltip"]').tooltip();

	/* ==========================================================================
      Form clear query strings
  ========================================================================== */
	$("form.disable-empty-fields")
		.on("submit", function() {
			$(this)
				.find(":input:not(button)")
				.filter(function() {
					return !this.value;
				})
				.prop("disabled", !0);
			return !0;
		})
		.find(":input:not(button)")
		.prop("disabled", !1);
});
