/**
 * API
 */

class Api {
  static getData(then) {
    return axios(config.AXIOS_DATA_POINT)
      .then(response => then(response.data.data))
      .catch()
  }
}

export default Api;