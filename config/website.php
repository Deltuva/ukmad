<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Web Config
    |--------------------------------------------------------------------------
    |
    */

    'footer_links' => [
        'Taisyklės' => '/taisykles',
        'Reklama' => '/reklama',
        'Kontaktai' => '/kontaktai',
        'D.U.K' => '/duk',
        'Prisijungti' => '/login',
        'Įsiminti skelbimai' => '#',
    ],

    'footer_created_by' => [
        'author' => 'Sprendimas: <a href = "#">Deltuva</a>',
    ],
];
